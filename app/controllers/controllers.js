'use strict';

angular.module('app.controllers', [])

  .controller('homeCtrl', function($scope) {

    $scope.login = {
      username: ''
    };

    if (localStorage.getItem('username')) {
      $scope.logged = true;
    }

    $scope.setUsername = function() {
      localStorage.setItem('username', $scope.login.username);
      $scope.logged = true;
    }
    
    $scope.chats = [
      { id: 1, name: "General" },
      { id: 2, name: "Videojuegos" },
      { id: 3, name: "Música" },
      { id: 4, name: "Twitter" },
      { id: 5, name: "Developers" }
    ];
  })

  .controller('chatCtrl', function($scope, $state, $filter, $stateParams, $firebaseArray) {

    if (!localStorage.getItem('username') || !$stateParams.name) {
      $state.go('home');
    }

    $scope.logout = function() {
      localStorage.clear();
      $state.go('home');
    }

    // chatroom name
    $scope.name = $stateParams.name;

    // initialize firebase
    var room = $filter('lowercase')($scope.name);
    var ref = firebase.database().ref('chats').child(room);
    var query = ref.limitToLast(50);

    $scope.data = $firebaseArray(query);

    // setting up our username
    $scope.me = localStorage.getItem('username');

    // var welcome = {
    //   username: "Bot",
    //   message: "Bienvenido a Firechat, " + username + "."
    // };

    // $scope.data.push(welcome);

    // some functions
    $scope.send = function(event) {
      if (event.key == "Enter" && $scope.msg != undefined) {
        var date = new Date().toISOString();
        var message = {
          username: $scope.me,
          message: $scope.msg,
          date: date
        };

        $scope.data.$add(message);
        $scope.msg = "";
      }
    }
    
  });