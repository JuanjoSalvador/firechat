angular.module('app.components', [])
    
    .directive('message', function () {
        return {
            restrict: 'E',
            scope: {
                msg: '=msg'
            },
            templateUrl: 'views/partials/message.html'
        }
    })

    .directive('schrollBottom', function () {
        return {
            scope: {
            schrollBottom: "="
            },
            link: function (scope, element) {
            scope.$watchCollection('schrollBottom', function (newValue) {
                if (newValue)
                {
                $(element).scrollTop($(element)[0].scrollHeight);
                }
            });
            }
        }
    });