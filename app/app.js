'use strict';

angular.module('app', ['app.controllers', 'app.services', 'app.components', 'ui.router', 'firebase', 'luegg.directives'])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state({
      name: 'home',
      url: '/',
      templateUrl: 'views/home.html',
      controller: 'homeCtrl'
    })

    .state({
      name: 'chat',
      url: '/chat/:id',
      templateUrl: 'views/chat.html',
      controller: 'chatCtrl',
      params: {
        id: null,
        name: null
      }
    })

    // By default
   $urlRouterProvider.when('', '/');
});